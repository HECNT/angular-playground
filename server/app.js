var express = require("express");
var app = express();
var bodyParser = require('body-parser');

var port = 3005

app.use(bodyParser.json());
app.use(bodyParser());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
})

app.get('/get-init', init);
app.post('/set', setSome);

function init(req, res) {
  var data = [
    {name: 'OSVALDO'},
    {name: 'JOSE'},
    {name: 'CARLOS'},
    {name: 'MARTIN'},
    {name: 'JESUS'}
  ]
  res.json({ err: false, data: data })
}

function setSome(req, res) {
  var d = req.body;
  if (d.usr === 'osva' && d.pass*1 === 1) {
    res.json({err: false, data: d})
  } else {
    res.json({err: true, data: d})
  }
}

app.listen(port, function() {
  console.log(`Escuchando en el puerto ${port}`);
})
