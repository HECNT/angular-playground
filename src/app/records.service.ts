import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface myData {
  obj: Object
}

@Injectable({
  providedIn: 'root'
})
export class RecordsService {

  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get<myData>('http://localhost:3005/get-init')
  }

  setSome(d) {
    return this.http.post<myData>('http://localhost:3005/set', d)
  }
}
