import { Component } from '@angular/core';
import { RecordsService } from './records.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app-playground';

  records = {}
  usr = { usr: "", pass: "" }

  constructor(private Service: RecordsService) {

  }

  onClick() {
    var d = this.usr;
    this.Service.setSome(d).subscribe(data => {
      console.log(data,'************++')
    })
  }

  ngOnInit() {
    this.Service.getData().subscribe(data => {
      console.log(data,'*********')
    })
  }

}
